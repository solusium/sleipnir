use {
    smallvec::SmallVec,
    syn::{Error, Signature, Type},
};

pub(crate) fn check_signature(signature: &Signature) -> SmallVec<[Error; 7]> {
    let checks = [
        (signature.asyncness.is_some(), "function must be async"),
        (
            signature.generics.params.is_empty(),
            "function must not be generic",
        ),
        (
            signature.generics.where_clause.is_none(),
            "function must not have `where` clauses",
        ),
        (
            signature.abi.is_none(),
            "function must not have an ABI qualifier",
        ),
        (
            signature.variadic.is_none(),
            "function must not be variadic",
        ),
        (
            1 == signature.inputs.len(),
            "function must have 1 argument: the handle.",
        ),
        (
            match &signature.output {
                syn::ReturnType::Default => true,
                syn::ReturnType::Type(_, ty) => match &**ty {
                    Type::Never(_) => true,
                    Type::Tuple(tuple) if tuple.elems.is_empty() => true,
                    _ => false,
                },
            },
            "function must either not return a value, return `()` or return `!`",
        ),
    ];

    checks
        .into_iter()
        .filter(|(okay, _)| !okay)
        .map(|(_, error_message)| Error::new_spanned(signature, error_message))
        .collect()
}
