#![warn(clippy::all, clippy::pedantic, clippy::nursery, clippy::cargo)]
#![allow(
    clippy::explicit_deref_methods,
    clippy::if_not_else,
    clippy::redundant_pub_crate
)]

mod macros;
mod utils;

use {
    crate::utils::check_signature::check_signature,
    syn::{parse_macro_input, ItemFn},
};

/// Allows to execute a async function by the selected runtime. This macro helps setting up a
/// Runtime without requiring the user to use Runtime directly.
#[proc_macro_attribute]
pub fn main(_: proc_macro::TokenStream, item: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let f = parse_macro_input!(item as ItemFn);

    crate::macros::main::build(&check_signature(&f.sig), f)
}

/// Allows to execute a async function by the selected runtime, suitable to the test environment.
/// This macro helps setting up a Runtime without requiring the user to use Runtime directly.
#[proc_macro_attribute]
pub fn test(_: proc_macro::TokenStream, item: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let f = parse_macro_input!(item as ItemFn);

    crate::macros::test::build(&check_signature(&f.sig), f)
}
