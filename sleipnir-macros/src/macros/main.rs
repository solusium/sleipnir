use {
    proc_macro::TokenStream,
    quote::{quote, ToTokens},
    syn::{Error, ItemFn},
};

pub(crate) fn build(errors: &[Error], f: ItemFn) -> TokenStream {
    if !errors.is_empty() {
        let compile_errors = errors.iter().map(syn::Error::to_compile_error);

        quote!(#(#compile_errors)*).into()
    } else {
        let handle = f.sig.inputs[0].clone().into_token_stream();

        let output = f.sig.output;

        let body = f.block;

        let code = quote!(
            async fn __sleipnir_main() #output
            {
                let #handle = ::sleipnir::runtime::Handle::try_current()
                    .expect("Failed getting a Runtime Handle");

                #body
            }

            unsafe fn __make_static<T>(t: &mut T) -> &'static mut T {
                ::core::mem::transmute(t)
            }

            fn main() #output
            {
                let mut runtime = ::sleipnir::runtime::Runtime::new()
                    .expect("Failed building the Runtime");

                let runtime = unsafe { __make_static(&mut runtime) };

                runtime.run(__sleipnir_main())
            }
        );

        quote!(#code).into()
    }
}
