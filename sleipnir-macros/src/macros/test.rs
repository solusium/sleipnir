use {
    proc_macro::TokenStream,
    quote::{quote, ToTokens},
    syn::{Error, Ident, ItemFn},
};

pub(crate) fn build(errors: &[Error], f: ItemFn) -> TokenStream {
    if !errors.is_empty() {
        let compile_errors = errors.iter().map(syn::Error::to_compile_error);

        quote!(#(#compile_errors)*).into()
    } else {
        let name = f.sig.ident.to_token_stream();

        let mut internal_function_name_string = "__sleipnir_".to_string();

        internal_function_name_string.push_str(f.sig.ident.to_string().trim_start_matches("r#"));

        let internal_function_name =
            Ident::new(&internal_function_name_string, f.sig.ident.span()).into_token_stream();

        let handle = f.sig.inputs[0].clone().into_token_stream();

        let output = f.sig.output;

        let body = f.block;

        let code = quote!(
            async fn #internal_function_name() #output
            {
                let #handle = ::sleipnir::runtime::Handle::try_current()
                    .expect("Failed getting a Runtime Handle");

                #body
            }

            #[test]
            fn #name() #output
            {
                let mut runtime = ::sleipnir::runtime::Runtime::new()
                    .expect("Failed building the Runtime");

                let runtime: &'static mut ::sleipnir::runtime::Runtime
                    = unsafe {::core::mem::transmute(&mut runtime) };

                runtime.run(#internal_function_name())
            }
        );

        quote!(#code).into()
    }
}
