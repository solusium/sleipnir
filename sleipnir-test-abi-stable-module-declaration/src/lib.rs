#[cfg(feature = "test")]
pub mod tests {
    use {
        abi_stable::{
            declare_root_module_statics,
            library::RootModule,
            package_version_strings,
            sabi_types::VersionStrings,
            std_types::{
                RResult,
                RResult::{RErr, ROk},
                RString,
            },
            StableAbi,
        },
        async_ffi::FfiFuture,
        sleipnir::ffi::{runtime::Handle, task::JoinHandle},
        std::{fmt::Debug, path::Path},
    };

    #[repr(C)]
    #[derive(StableAbi)]
    #[sabi(kind(Prefix(prefix_fields = ModulePrefix, prefix_ref = ModuleRef)))]
    pub struct Module {
        pub current: extern "C" fn() -> RResult<Handle, RString>,

        #[sabi(last_prefix_field)]
        pub spawn: extern "C" fn(Handle, FfiFuture<i32>) -> JoinHandle<i32>,
    }

    pub fn current(path: &Path) -> Result<Handle, String> {
        let module_result = ModuleRef::load_from_file(path);

        assert!(module_result.is_ok());

        let module = module_result.unwrap();

        match module.current()() {
            ROk(value) => Ok(value),
            RErr(error) => Err(error.into()),
        }
    }

    #[inline]
    pub fn spawn(path: &Path, handle: Handle, future: FfiFuture<i32>) -> JoinHandle<i32> {
        let module_result = ModuleRef::load_from_file(path);

        assert!(module_result.is_ok());

        let module = module_result.unwrap();

        module.spawn()(handle, future)
    }

    impl Debug for ModuleRef {
        #[inline]
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "Module")
        }
    }

    impl RootModule for ModuleRef {
        declare_root_module_statics! {ModuleRef}

        const BASE_NAME: &'static str = "sleipnir-test-abi-stable-module-declaration";

        const NAME: &'static str = "sleipnir-test-abi-stable-module-declaration";

        const VERSION_STRINGS: VersionStrings = package_version_strings!();
    }
}
