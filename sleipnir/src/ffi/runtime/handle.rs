use {
    crate::ffi::{
        runtime::handle::stable_abi::{Trait, Trait_TO},
        task::JoinHandle,
    },
    abi_stable::{sabi_trait::TD_Opaque, std_types::RArc, StableAbi},
    async_ffi::{FfiFuture, FutureExt},
    bytemuck::Pod,
    core::fmt::Debug,
    futures::channel::oneshot::channel,
};

pub(super) mod stable_abi {
    #![allow(
        clippy::used_underscore_binding,
        clippy::ptr_as_ptr,
        clippy::cast_ptr_alignment,
        improper_ctypes_definitions,
        non_local_definitions
    )]

    use {abi_stable::sabi_trait, async_ffi::FfiFuture};

    #[sabi_trait]
    pub(super) trait Trait: Clone + Debug + Send + Sync + 'static {
        fn spawn(&self, future: FfiFuture<()>);
    }
}

#[derive(Clone, Debug)]
struct Implementation {
    handle: crate::runtime::Handle,
}

/// FFI-safe wrapper for [`Handle`](crate::runtime::Handle).
#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
pub struct Handle {
    implementation: Trait_TO<RArc<()>>,
}

impl Handle {
    /// Constructs a Handle from a [`Handle`](crate::runtime::Handle).
    #[inline]
    #[must_use]
    pub fn new(handle: crate::runtime::Handle) -> Self {
        let implementation = Trait_TO::from_ptr(RArc::new(Implementation { handle }), TD_Opaque);

        Self { implementation }
    }

    /// Spawns a future onto the runtime.
    #[inline]
    pub fn spawn<F>(&self, future: F) -> JoinHandle<F::Output>
    where
        F: core::future::Future + Send + 'static,
        F::Output: Debug + Pod + Send + StableAbi + 'static,
        // <F as futures::Future>::Output: Sync,
    {
        let (sender, receiver) = channel();

        self.implementation.spawn(
            async move {
                let _result = sender.send(future.await);
            }
            .into_ffi(),
        );

        JoinHandle::new(receiver.into())
    }
}

impl From<crate::runtime::Handle> for Handle {
    #[inline]
    fn from(handle: crate::runtime::Handle) -> Self {
        Self::new(handle)
    }
}

impl Trait for Implementation {
    #[inline]
    fn spawn(&self, future: FfiFuture<()>) {
        let _join_handle = self.handle.spawn(future);
    }
}
