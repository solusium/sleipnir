use {
    abi_stable::StableAbi,
    core::fmt::{Debug, Display, Formatter, Result},
};
pub mod broadcast;
pub mod mpsc;
pub mod oneshot;

/// The error type for sync operations.
#[repr(C)]
#[derive(Clone, Eq, PartialEq, StableAbi)]
pub enum Error {
    /// All instances of the other channel component dropped.
    Disconnected,
    /// Trying to send a value through the channel while full.
    Full,
    /// A future panicked.
    Panic,
}

impl Debug for Error {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        match self {
            Self::Disconnected => {
                f.write_str("all instances of the other channel component dropped")
            }
            Self::Full => f.write_str("trying to send a value through the channel while full"),
            Self::Panic => f.write_str("a future panicked"),
        }
    }
}

impl Display for Error {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        Debug::fmt(self, f)
    }
}

unsafe impl Send for Error {}

unsafe impl Sync for Error {}
