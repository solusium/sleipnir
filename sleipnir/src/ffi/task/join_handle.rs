use {
    crate::{ffi::sync::oneshot::Receiver, Error},
    abi_stable::StableAbi,
    bytemuck::Pod,
    core::{
        fmt::Debug,
        future::Future,
        pin::{pin, Pin},
        task::{Context, Poll},
    },
};

/// FFI-safe wrapper for [`JoinHandle`](crate::task::JoinHandle).
#[repr(C)]
#[derive(StableAbi)]
pub struct JoinHandle<T: Pod + Send + StableAbi + 'static> {
    receiver: Receiver<T>,
}

impl<T> JoinHandle<T>
where
    T: Pod + Send + StableAbi + 'static,
{
    #[inline]
    pub(crate) const fn new(receiver: Receiver<T>) -> Self {
        Self { receiver }
    }
}

impl<T> Future for JoinHandle<T>
where
    T: Debug + Pod + Send + StableAbi + 'static,
{
    type Output = Result<T, Error>;

    #[inline]
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.get_mut();

        let receiver = &mut this.receiver;

        let pinned_receiver = pin!(receiver);

        match pinned_receiver.poll(cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(result) => match result {
                Ok(value) => Poll::Ready(Ok(value)),
                Err(error) => Poll::Ready(Err(match error {
                    crate::ffi::sync::Error::Disconnected => Error::Cancelled,
                    crate::ffi::sync::Error::Full => unreachable!(),
                    crate::ffi::sync::Error::Panic => Error::TaskPanic,
                })),
            },
        }
    }
}
