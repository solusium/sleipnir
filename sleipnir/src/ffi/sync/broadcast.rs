pub use crate::ffi::sync::broadcast::{receiver::Receiver, sender::Sender};
use {abi_stable::StableAbi, core::fmt::Debug};

mod receiver;
mod sender;

/// Create a bounded, multi-producer, multi-consumer channel where we broadcast each value to all
/// active receivers.
#[inline]
#[must_use]
pub fn channel<T>(buffer: usize) -> (Sender<T>, Receiver<T>)
where
    T: Clone + Debug + Send + StableAbi + Sync + 'static,
{
    let (sender, receiver) = tokio::sync::broadcast::channel(buffer);

    (sender.into(), receiver.into())
}
