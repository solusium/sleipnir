pub use crate::ffi::sync::mpsc::{
    receiver::Receiver, sender::Sender, unbounded_receiver::UnboundedReceiver,
    unbounded_sender::UnboundedSender,
};
use {abi_stable::StableAbi, core::fmt::Debug};

mod receiver;
mod sender;
mod unbounded_receiver;
mod unbounded_sender;

/// Constructs a bounded mpsc channel for communicating between asynchronous tasks.
#[inline]
#[must_use]
pub fn channel<T>(buffer: usize) -> (Sender<T>, Receiver<T>)
where
    T: Clone + Debug + Send + StableAbi + 'static,
{
    let (sender, receiver) = futures::channel::mpsc::channel(buffer);

    (sender.into(), receiver.into())
}

/// Constructs an unbounded mpsc channel for communicating between asynchronous tasks.
#[inline]
#[must_use]
pub fn unbounded<T>() -> (UnboundedSender<T>, UnboundedReceiver<T>)
where
    T: Clone + Debug + Send + StableAbi + 'static,
{
    let (sender, receiver) = futures::channel::mpsc::unbounded();

    (sender.into(), receiver.into())
}
