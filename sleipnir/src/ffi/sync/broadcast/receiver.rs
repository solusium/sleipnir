use {
    crate::ffi::sync::broadcast::receiver::stable_abi::{Trait, Trait_TO},
    abi_stable::{
        marker_type::NonOwningPhantom,
        sabi_trait::TD_Opaque,
        std_types::{
            RBox,
            ROption::{self, RNone, RSome},
            RSlice,
        },
        StableAbi,
    },
    async_ffi::{FfiFuture, FutureExt},
    core::{
        fmt::{Debug, Formatter},
        future::Future,
        mem::{size_of, transmute},
        pin::{pin, Pin},
        slice::from_raw_parts,
        task::{Context, Poll},
    },
    futures::Stream,
};

pub(super) mod stable_abi {
    #![allow(
        clippy::used_underscore_binding,
        clippy::ptr_as_ptr,
        clippy::cast_ptr_alignment,
        non_local_definitions
    )]

    use {
        abi_stable::{
            sabi_trait,
            std_types::{RBox, ROption, RSlice},
        },
        async_ffi::FfiFuture,
    };

    #[sabi_trait]
    pub(crate) trait Trait: Debug + Send + 'static {
        fn next(&mut self) -> FfiFuture<ROption<RSlice<'static, u8>>>;

        fn resubscribe(&self) -> Trait_TO<RBox<()>>;
    }
}

#[derive(Debug)]
pub(super) struct Inner<T> {
    receiver: tokio::sync::broadcast::Receiver<T>,
    next: Option<T>,
}

/// The receiving end of a bounded broadcast channel, created by the
/// [`channel`](crate::ffi::sync::broadcast::channel) function.
#[repr(C)]
#[derive(StableAbi)]
pub struct Receiver<T>
where
    T: StableAbi,
{
    inner: Trait_TO<RBox<()>>,
    next: ROption<FfiFuture<ROption<RSlice<'static, u8>>>>,
    _phantom_data: NonOwningPhantom<T>,
}

impl<T> Inner<T>
where
    T: Clone + Debug + Send,
{
    #[inline]
    pub(super) const fn new(receiver: tokio::sync::broadcast::Receiver<T>) -> Self {
        let next = None;

        Self { receiver, next }
    }

    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn next(&mut self) -> ROption<RSlice<'_, u8>> {
        match self.receiver.recv().await {
            Err(_) => RNone,
            Ok(value) => {
                let next: *const T = self.next.insert(value);

                // we cast this back to T in [`Receiver::poll_next`]
                RSome(unsafe { from_raw_parts(next.cast(), size_of::<T>()).into() })
            }
        }
    }
}

impl<T> Receiver<T>
where
    T: StableAbi,
{
    #[inline]
    pub(super) const fn new(inner: Trait_TO<RBox<()>>) -> Self {
        let next = RNone;

        Self {
            inner,
            next,
            _phantom_data: NonOwningPhantom::NEW,
        }
    }

    /// Re-subscribes to the channel, receiving all messages after this function call.
    #[inline]
    #[must_use]
    pub fn resubscribe(&self) -> Self {
        let inner = self.inner.resubscribe();
        let next = RNone;

        Self {
            inner,
            next,
            _phantom_data: NonOwningPhantom::NEW,
        }
    }
}

impl<T> Debug for Receiver<T>
where
    T: Debug + StableAbi,
{
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("Receiver")
            .field("inner", &self.inner)
            .finish_non_exhaustive()
    }
}

impl<T> From<tokio::sync::broadcast::Receiver<T>> for Receiver<T>
where
    T: Clone + Debug + Send + StableAbi + Sync + 'static,
{
    #[inline]
    fn from(receiver: tokio::sync::broadcast::Receiver<T>) -> Self {
        let inner = Trait_TO::from_value(
            Inner {
                receiver,
                next: None,
            },
            TD_Opaque,
        );

        Self {
            inner,
            next: RNone,
            _phantom_data: NonOwningPhantom::NEW,
        }
    }
}

impl<T> Trait for Inner<T>
where
    T: Clone + Debug + Send + Sync + 'static,
{
    #[inline]
    fn next(&mut self) -> FfiFuture<ROption<RSlice<'static, u8>>> {
        // we poll the future in UnboundedReceiver::poll_next
        unsafe { transmute(async move { self.next().await }.into_ffi()) }
    }

    #[inline]
    fn resubscribe(&self) -> Trait_TO<RBox<()>> where {
        let receiver = self.receiver.resubscribe();

        let next = None;

        Trait_TO::from_value(Self { receiver, next }, TD_Opaque)
    }
}

unsafe impl<T> Send for Receiver<T> where T: Send + StableAbi {}

impl<T> Stream for Receiver<T>
where
    T: Clone + Debug + Send + StableAbi + Unpin + 'static,
{
    type Item = T;

    #[inline]
    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let this = self.get_mut();

        let future = this.next.get_or_insert_with(|| this.inner.next());

        let pinned_future = pin!(future);

        match pinned_future.poll(cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(maybe_bytes) => {
                let _ = this.next.take();

                match maybe_bytes {
                    RNone => Poll::Ready(None),
                    RSome(bytes) => {
                        Poll::Ready(Some(
                            // we constructed this slice from an instance of T in Inner::next
                            unsafe { bytes.as_ptr().cast::<T>().as_ref() }
                                .expect("pointer must not be null")
                                .clone(),
                        ))
                    }
                }
            }
        }
    }
}

unsafe impl<T> Sync for Receiver<T> where T: Send + StableAbi {}

impl<T> Unpin for Receiver<T> where T: Debug + StableAbi {}
