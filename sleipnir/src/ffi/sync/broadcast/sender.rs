use {
    crate::ffi::sync::{
        broadcast::{
            sender::stable_abi::{Trait, Trait_TO},
            Receiver,
        },
        Error,
    },
    abi_stable::{
        marker_type::NonOwningPhantom,
        sabi_trait::TD_Opaque,
        std_types::{
            RBox,
            RResult::{self, RErr, ROk},
            RSlice,
        },
        StableAbi,
    },
    core::{
        fmt::{Debug, Formatter},
        mem::size_of,
        slice::from_raw_parts,
    },
};

mod stable_abi {
    #![allow(
        clippy::used_underscore_binding,
        clippy::ptr_as_ptr,
        clippy::cast_ptr_alignment,
        non_local_definitions
    )]

    use {
        crate::ffi::sync::Error,
        abi_stable::{
            sabi_trait,
            std_types::{RBox, RResult, RSlice},
        },
    };

    #[sabi_trait]
    pub(super) trait Trait: Debug + Send + 'static {
        fn clone(&self) -> Trait_TO<RBox<()>>;

        fn send(&self, bytes: RSlice<'_, u8>) -> RResult<usize, Error>;

        fn subscribe(
            &self,
        ) -> crate::ffi::sync::broadcast::receiver::stable_abi::Trait_TO<RBox<()>>;
    }
}

#[derive(Debug)]
struct Inner<T> {
    sender: tokio::sync::broadcast::Sender<T>,
}

/// The transmission end of a bounded broadcast channel, created by the
/// [`channel`](crate::ffi::sync::broadcast::channel) function.
#[repr(C)]
#[derive(StableAbi)]
pub struct Sender<T>
where
    T: StableAbi,
{
    inner: Trait_TO<RBox<()>>,
    _phantom_data: NonOwningPhantom<T>,
}

impl<T> Sender<T>
where
    T: Send + StableAbi + Sync,
{
    /// Attempting to send a message to all [`Receivers`](crate::ffi::sync::broadcast::receiver::Receiver).
    ///
    /// # Errors
    ///
    /// If all Receivers dropped, this returns [`Error::Disconnected`].
    pub fn send(&self, message: T) -> Result<usize, Error> {
        let pointer: *const T = &message;

        // We will cast this back to T in Inner::next (as Trait).
        let bytes = unsafe { from_raw_parts(pointer.cast(), size_of::<T>()) }.into();

        self.inner.send(bytes).into()
    }

    /// Constructs a new [`Receivers`](crate::ffi::sync::broadcast::receiver::Receiver) that will
    /// receive values sent after this call to subscribe.
    #[inline]
    #[must_use]
    pub fn subscribe(&self) -> Receiver<T> {
        Receiver::new(self.inner.subscribe())
    }
}

impl<T> Trait for Inner<T>
where
    T: Clone + Debug + Send + Sync + 'static,
{
    #[inline]
    fn clone(&self) -> Trait_TO<RBox<()>> {
        let sender = self.sender.clone();

        Trait_TO::from_value(Self { sender }, TD_Opaque)
    }

    #[inline]
    fn send(&self, bytes: RSlice<'_, u8>) -> RResult<usize, Error> {
        let send_result = self.sender.send(
            // We created bytes from an instance of T in Sender::send.
            unsafe { bytes.as_ptr().cast::<T>().as_ref() }
                .expect("pointer must not be null")
                .clone(),
        );

        send_result.map_or(RErr(Error::Disconnected), ROk)
    }

    #[inline]
    fn subscribe(&self) -> crate::ffi::sync::broadcast::receiver::stable_abi::Trait_TO<RBox<()>> {
        crate::ffi::sync::broadcast::receiver::stable_abi::Trait_TO::from_value(
            crate::ffi::sync::broadcast::receiver::Inner::new(self.sender.subscribe()),
            TD_Opaque,
        )
    }
}

impl<T> Clone for Sender<T>
where
    T: StableAbi,
{
    #[inline]
    fn clone(&self) -> Self {
        let inner = self.inner.clone();

        Self {
            inner,
            _phantom_data: NonOwningPhantom::NEW,
        }
    }
}

impl<T> Debug for Sender<T>
where
    T: Debug + StableAbi,
{
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("Sender")
            .field("inner", &self.inner)
            .finish_non_exhaustive()
    }
}

impl<T> From<tokio::sync::broadcast::Sender<T>> for Sender<T>
where
    T: Clone + Debug + Send + StableAbi + Sync + 'static,
{
    #[inline]
    fn from(sender: tokio::sync::broadcast::Sender<T>) -> Self {
        let inner = Trait_TO::from_value(Inner { sender }, TD_Opaque);

        Self {
            inner,
            _phantom_data: NonOwningPhantom::NEW,
        }
    }
}

unsafe impl<T> Send for Sender<T> where T: Send + StableAbi {}
unsafe impl<T> Sync for Sender<T> where T: Sync + StableAbi {}

impl<T> Unpin for Sender<T> where T: StableAbi {}
