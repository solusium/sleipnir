use {
    crate::ffi::sync::{
        oneshot::sender::stable_abi::{Trait, Trait_TO},
        Error,
    },
    abi_stable::{
        marker_type::NonOwningPhantom,
        sabi_trait::TD_Opaque,
        std_types::{
            RBox,
            RResult::{self, RErr, ROk},
            RSlice,
        },
        StableAbi,
    },
    core::{
        fmt::{Debug, Formatter},
        mem::size_of,
        slice::from_raw_parts,
    },
};

mod stable_abi {
    #![allow(
        clippy::used_underscore_binding,
        clippy::ptr_as_ptr,
        clippy::cast_ptr_alignment,
        clippy::unnecessary_cast,
        non_local_definitions
    )]

    use {
        crate::ffi::sync::Error,
        abi_stable::{
            sabi_trait,
            std_types::{RResult, RSlice},
        },
    };

    #[sabi_trait]
    pub(super) trait Trait: Debug + Send + 'static {
        fn send(self, bytes: RSlice<'_, u8>) -> RResult<(), Error>;
    }
}

#[derive(Debug)]
struct Inner<T> {
    sender: futures::channel::oneshot::Sender<T>,
}

/// The transmission end of an one-shot channel, created by the
/// [`channel`](crate::ffi::sync::oneshot::channel) function.
#[repr(C)]
#[derive(StableAbi)]
pub struct Sender<T>
where
    T: StableAbi,
{
    inner: Trait_TO<RBox<()>>,
    _phantom_data: NonOwningPhantom<T>,
}

impl<T> Sender<T>
where
    T: StableAbi,
{
    /// Try to send a value through this channel.
    ///
    /// # Errors
    ///
    /// If the receiving component of this channel dropped, this returns [`Error::Disconnected`].
    #[inline]
    pub fn send(self, message: T) -> Result<(), Error> {
        let pointer: *const T = &message;

        self.inner
            .send(unsafe { from_raw_parts(pointer.cast(), size_of::<T>()) }.into())
            .into()
    }
}

impl<T> Trait for Inner<T>
where
    T: Clone + Debug + Send + 'static,
{
    fn send(self, bytes: RSlice<'_, u8>) -> RResult<(), Error> {
        let send_result = self.sender.send(
            unsafe { bytes.as_ptr().cast::<T>().as_ref() }
                .expect("pointer must not be null")
                .clone(),
        );

        match send_result {
            Ok(()) => ROk(()),
            Err(_) => RErr(Error::Disconnected),
        }
    }
}

impl<T> Debug for Sender<T>
where
    T: Debug + StableAbi,
{
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("Sender")
            .field("inner", &self.inner)
            .finish_non_exhaustive()
    }
}

impl<T> From<futures::channel::oneshot::Sender<T>> for Sender<T>
where
    T: Clone + Debug + Send + StableAbi + 'static,
{
    #[inline]
    fn from(sender: futures::channel::oneshot::Sender<T>) -> Self {
        let inner = Trait_TO::from_value(Inner { sender }, TD_Opaque);

        Self {
            inner,
            _phantom_data: NonOwningPhantom::NEW,
        }
    }
}

unsafe impl<T> Send for Sender<T> where T: Send + StableAbi {}
unsafe impl<T> Sync for Sender<T> where T: Sync + StableAbi {}

impl<T> Unpin for Sender<T> where T: StableAbi {}
