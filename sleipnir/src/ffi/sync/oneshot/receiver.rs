use {
    crate::ffi::sync::{
        oneshot::receiver::stable_abi::{Trait, Trait_TO},
        Error,
    },
    abi_stable::{
        marker_type::NonOwningPhantom,
        sabi_trait::TD_Opaque,
        std_types::{
            RBox,
            ROption::{self, RNone},
            RResult::{self, RErr, ROk},
            RSlice,
        },
        StableAbi,
    },
    async_ffi::{FfiFuture, FutureExt},
    core::{
        fmt::{Debug, Formatter},
        future::Future,
        mem::{size_of, transmute},
        pin::{pin, Pin},
        slice::from_raw_parts,
        task::{Context, Poll},
    },
};

mod stable_abi {
    #![allow(
        clippy::used_underscore_binding,
        clippy::ptr_as_ptr,
        clippy::cast_ptr_alignment,
        non_local_definitions
    )]

    use {
        crate::ffi::sync::Error,
        abi_stable::{
            sabi_trait,
            std_types::{RResult, RSlice},
        },
        async_ffi::FfiFuture,
    };

    #[sabi_trait]
    pub(super) trait Trait: Debug + Send + 'static {
        fn recv(&mut self) -> FfiFuture<RResult<RSlice<'static, u8>, Error>>;
    }
}

#[derive(Debug)]
struct Inner<T> {
    receiver: Option<futures::channel::oneshot::Receiver<T>>,
    next: Option<T>,
}

/// The receiving end of a bounded mpsc channel, created by the
/// [`channel`](crate::ffi::sync::mpsc::channel) function.
#[repr(C)]
#[derive(StableAbi)]
pub struct Receiver<T>
where
    T: StableAbi,
{
    inner: Trait_TO<RBox<()>>,
    next: ROption<FfiFuture<RResult<RSlice<'static, u8>, Error>>>,
    _phantom_data: NonOwningPhantom<T>,
}

impl<T> Inner<T>
where
    T: Debug + Send,
{
    #[inline]
    async fn recv(&mut self) -> RResult<RSlice<'static, u8>, Error> {
        let receiver = self.receiver.take().expect("calling recv more than once");

        match receiver.await {
            Err(_) => RErr(Error::Disconnected),
            Ok(value) => {
                let next: *const T = self.next.insert(value);

                // we cast this back to T in [`Receiver::poll_next`]
                ROk(unsafe { from_raw_parts(next.cast(), size_of::<T>()).into() })
            }
        }
    }
}

impl<T> Debug for Receiver<T>
where
    T: Debug + StableAbi,
{
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("Receiver")
            .field("inner", &self.inner)
            .finish_non_exhaustive()
    }
}

impl<T> From<futures::channel::oneshot::Receiver<T>> for Receiver<T>
where
    T: Debug + Send + StableAbi + 'static,
{
    #[inline]
    fn from(receiver: futures::channel::oneshot::Receiver<T>) -> Self {
        let receiver = Some(receiver);

        let inner = Trait_TO::from_value(
            Inner {
                receiver,
                next: None,
            },
            TD_Opaque,
        );

        Self {
            inner,
            next: RNone,
            _phantom_data: NonOwningPhantom::NEW,
        }
    }
}

impl<T> Trait for Inner<T>
where
    T: Debug + Send + 'static,
{
    #[inline]
    fn recv(&mut self) -> FfiFuture<RResult<RSlice<'static, u8>, Error>> {
        // we poll the future in UnboundedReceiver::poll_next
        unsafe { transmute(async move { self.recv().await }.into_ffi()) }
    }
}

impl<T> Future for Receiver<T>
where
    T: Clone + Debug + Send + StableAbi + 'static,
{
    type Output = Result<T, Error>;

    #[inline]
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.get_mut();

        let future = this.next.get_or_insert_with(|| this.inner.recv());

        let pinned_future = pin!(future);

        match pinned_future.poll(cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(bytes_result) => {
                let _ = this.next.take();

                match bytes_result {
                    RErr(error) => Poll::Ready(Err(error)),
                    ROk(bytes) => {
                        Poll::Ready(Ok(
                            // we constructed this slice from an instance of T in Inner::recv
                            unsafe { bytes.as_ptr().cast::<T>().as_ref() }
                                .expect("pointer must not be null")
                                .clone(),
                        ))
                    }
                }
            }
        }
    }
}

unsafe impl<T> Send for Receiver<T> where T: Send + StableAbi {}

unsafe impl<T> Sync for Receiver<T> where T: Send + StableAbi {}

impl<T> Unpin for Receiver<T> where T: Debug + StableAbi {}
