pub use crate::ffi::sync::oneshot::{receiver::Receiver, sender::Sender};
use {abi_stable::StableAbi, core::fmt::Debug};

mod receiver;
mod sender;

/// Creates a new one-shot channel for sending a single value across asynchronous tasks.
#[inline]
#[must_use]
pub fn channel<T>() -> (Sender<T>, Receiver<T>)
where
    T: Clone + Debug + Send + StableAbi + 'static,
{
    let (sender, receiver) = futures::channel::oneshot::channel();

    (sender.into(), receiver.into())
}
