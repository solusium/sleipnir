use {
    crate::ffi::sync::{
        mpsc::sender::stable_abi::{Trait, Trait_TO},
        Error,
    },
    abi_stable::{
        marker_type::NonOwningPhantom,
        sabi_trait::TD_Opaque,
        std_types::{
            RBox,
            RResult::{self, RErr, ROk},
            RSlice,
        },
        StableAbi,
    },
    async_ffi::{ContextExt, FfiContext, FfiPoll},
    core::{
        fmt::{Debug, Formatter},
        mem::size_of,
        pin::{pin, Pin},
        slice::from_raw_parts,
        task::{Context, Poll},
    },
    futures::{channel::mpsc::SendError, Sink},
};

mod stable_abi {
    #![allow(
        clippy::used_underscore_binding,
        clippy::ptr_as_ptr,
        clippy::cast_ptr_alignment,
        non_local_definitions
    )]

    use {
        crate::ffi::sync::Error,
        abi_stable::{
            sabi_trait,
            std_types::{RBox, RResult, RSlice},
        },
        async_ffi::{FfiContext, FfiPoll},
    };

    #[sabi_trait]
    pub(super) trait Trait: Debug + Send + 'static {
        fn clone(&self) -> Trait_TO<RBox<()>>;

        fn poll_close(&mut self, ffi_cx: &mut FfiContext) -> FfiPoll<RResult<(), Error>>;

        fn poll_flush(&mut self, ffi_cx: &mut FfiContext) -> FfiPoll<RResult<(), Error>>;

        fn poll_ready(&mut self, ffi_cx: &mut FfiContext<'_>) -> FfiPoll<RResult<(), Error>>;

        fn start_send(&mut self, bytes: RSlice<'_, u8>) -> RResult<(), Error>;
    }
}

#[derive(Debug)]
struct Inner<T> {
    sender: futures::channel::mpsc::Sender<T>,
}

/// The transmission end of a bounded mpsc channel, created by the
/// [`channel`](crate::ffi::sync::mpsc::channel) function.
#[repr(C)]
#[derive(StableAbi)]
pub struct Sender<T>
where
    T: StableAbi,
{
    inner: Trait_TO<RBox<()>>,
    _phantom_data: NonOwningPhantom<T>,
}

impl<T> Inner<T> {
    #[inline]
    fn cast_poll_result(poll_result: Poll<Result<(), SendError>>) -> FfiPoll<RResult<(), Error>> {
        match poll_result {
            Poll::Pending => FfiPoll::Pending,
            Poll::Ready(result) => match result {
                Ok(()) => FfiPoll::Ready(ROk(())),
                Err(error) => {
                    if error.is_disconnected() {
                        FfiPoll::Ready(RErr(Error::Disconnected))
                    } else {
                        FfiPoll::Ready(RErr(Error::Full))
                    }
                }
            },
        }
    }
}

impl<T> Sender<T>
where
    T: StableAbi,
{
    fn cast_poll_result(poll_result: FfiPoll<RResult<(), Error>>) -> Poll<Result<(), Error>> {
        match poll_result {
            FfiPoll::Ready(result) => Poll::Ready(result.into()),
            FfiPoll::Pending => Poll::Pending,
            FfiPoll::Panicked => Poll::Ready(Err(Error::Panic)),
        }
    }
}

impl<T> Trait for Inner<T>
where
    T: Clone + Debug + Send + 'static,
{
    #[inline]
    fn clone(&self) -> Trait_TO<RBox<()>> {
        let sender = self.sender.clone();

        Trait_TO::from_value(Self { sender }, TD_Opaque)
    }

    #[inline]
    fn poll_close(&mut self, ffi_cx: &mut FfiContext) -> FfiPoll<RResult<(), Error>> where {
        Self::cast_poll_result(ffi_cx.with_context(|cx| {
            let sender = &mut self.sender;

            let pinned_sender = pin!(sender);

            pinned_sender.poll_close(cx)
        }))
    }

    #[inline]
    fn poll_flush(&mut self, ffi_cx: &mut FfiContext) -> FfiPoll<RResult<(), Error>> where {
        Self::cast_poll_result(ffi_cx.with_context(|cx| {
            let sender = &mut self.sender;

            let pinned_sender = pin!(sender);

            pinned_sender.poll_flush(cx)
        }))
    }

    fn poll_ready(&mut self, ffi_cx: &mut FfiContext<'_>) -> FfiPoll<RResult<(), Error>> where {
        Self::cast_poll_result(ffi_cx.with_context(|cx| {
            let sender = &mut self.sender;

            let pinned_sender = pin!(sender);

            pinned_sender.poll_ready(cx)
        }))
    }

    fn start_send(&mut self, bytes: RSlice<'_, u8>) -> RResult<(), Error> where {
        let send_result = self.sender.start_send(
            unsafe { bytes.as_ptr().cast::<T>().as_ref() }
                .expect("pointer must not be null")
                .clone(),
        );

        match send_result {
            Ok(()) => ROk(()),
            Err(error) => RErr(if error.is_disconnected() {
                Error::Disconnected
            } else {
                Error::Full
            }),
        }
    }
}

impl<T> Clone for Sender<T>
where
    T: StableAbi,
{
    #[inline]
    fn clone(&self) -> Self {
        let inner = self.inner.clone();

        Self {
            inner,
            _phantom_data: NonOwningPhantom::NEW,
        }
    }
}

impl<T> Debug for Sender<T>
where
    T: Debug + StableAbi,
{
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("Sender")
            .field("inner", &self.inner)
            .finish_non_exhaustive()
    }
}

impl<T> From<futures::channel::mpsc::Sender<T>> for Sender<T>
where
    T: Clone + Debug + Send + StableAbi + 'static,
{
    #[inline]
    fn from(sender: futures::channel::mpsc::Sender<T>) -> Self {
        let inner = Trait_TO::from_value(Inner { sender }, TD_Opaque);

        Self {
            inner,
            _phantom_data: NonOwningPhantom::NEW,
        }
    }
}

impl<T> Sink<T> for Sender<T>
where
    T: StableAbi,
{
    type Error = Error;

    #[inline]
    fn poll_close(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Self::cast_poll_result(
            cx.with_ffi_context(|ffi_cx| self.get_mut().inner.poll_close(ffi_cx)),
        )
    }

    fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Self::cast_poll_result(
            cx.with_ffi_context(|ffi_cx| self.get_mut().inner.poll_flush(ffi_cx)),
        )
    }

    fn poll_ready(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Self::cast_poll_result(
            cx.with_ffi_context(|ffi_cx| self.get_mut().inner.poll_flush(ffi_cx)),
        )
    }

    fn start_send(self: Pin<&mut Self>, item: T) -> Result<(), Self::Error> {
        let pointer: *const T = &item;

        let result = self
            .get_mut()
            .inner
            .start_send(unsafe { from_raw_parts(pointer.cast(), size_of::<T>()) }.into());

        match result {
            ROk(()) => Ok(()),
            RErr(error) => Err(error),
        }
    }
}

unsafe impl<T> Send for Sender<T> where T: Send + StableAbi {}
unsafe impl<T> Sync for Sender<T> where T: Sync + StableAbi {}

impl<T> Unpin for Sender<T> where T: StableAbi {}
