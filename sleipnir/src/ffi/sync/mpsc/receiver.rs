use {
    crate::ffi::sync::mpsc::receiver::stable_abi::{Trait, Trait_TO},
    abi_stable::{
        marker_type::NonOwningPhantom,
        sabi_trait::TD_Opaque,
        std_types::{
            RBox,
            ROption::{self, RNone, RSome},
            RSlice,
        },
        StableAbi,
    },
    async_ffi::{FfiFuture, FutureExt},
    core::{
        fmt::{Debug, Formatter},
        future::Future,
        mem::{size_of, transmute},
        pin::{pin, Pin},
        slice::from_raw_parts,
        task::{Context, Poll},
    },
    futures::{Stream, StreamExt},
};

mod stable_abi {
    #![allow(
        clippy::used_underscore_binding,
        clippy::ptr_as_ptr,
        clippy::cast_ptr_alignment,
        non_local_definitions
    )]

    use {
        abi_stable::{
            sabi_trait,
            std_types::{ROption, RSlice},
        },
        async_ffi::FfiFuture,
    };

    #[sabi_trait]
    pub(super) trait Trait: Debug + Send + 'static {
        fn close(&mut self);

        fn next(&mut self) -> FfiFuture<ROption<RSlice<'static, u8>>>;
    }
}

#[derive(Debug)]
struct Inner<T> {
    receiver: futures::channel::mpsc::Receiver<T>,
    next: Option<T>,
}

/// The receiving end of a bounded mpsc channel, created by the
/// [`channel`](crate::ffi::sync::mpsc::channel) function.
#[repr(C)]
#[derive(StableAbi)]
pub struct Receiver<T>
where
    T: StableAbi,
{
    inner: Trait_TO<RBox<()>>,
    next: ROption<FfiFuture<ROption<RSlice<'static, u8>>>>,
    _phantom_data: NonOwningPhantom<T>,
}

impl<T> Inner<T>
where
    T: Debug + Send,
{
    #[inline]
    #[cfg_attr(feature = "async-backtrace", async_backtrace::framed)]
    async fn next(&mut self) -> ROption<RSlice<'_, u8>> {
        match self.receiver.next().await {
            None => RNone,
            Some(value) => {
                let next: *const T = self.next.insert(value);

                // we cast this back to T in [`Receiver::poll_next`]
                RSome(unsafe { from_raw_parts(next.cast(), size_of::<T>()).into() })
            }
        }
    }
}

impl<T> Receiver<T>
where
    T: StableAbi,
{
    /// Closes the receiving half of a channel, without dropping it.
    ///
    /// This prevents sending any further messages on the channel while still enabling the receiver
    /// to drain buffered messages.
    #[inline]
    pub fn close(&mut self) {
        self.inner.close();
    }
}

impl<T> Debug for Receiver<T>
where
    T: Debug + StableAbi,
{
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("Receiver")
            .field("inner", &self.inner)
            .finish_non_exhaustive()
    }
}

impl<T> From<futures::channel::mpsc::Receiver<T>> for Receiver<T>
where
    T: Debug + Send + StableAbi + 'static,
{
    #[inline]
    fn from(receiver: futures::channel::mpsc::Receiver<T>) -> Self {
        let inner = Trait_TO::from_value(
            Inner {
                receiver,
                next: None,
            },
            TD_Opaque,
        );

        Self {
            inner,
            next: RNone,
            _phantom_data: NonOwningPhantom::NEW,
        }
    }
}

impl<T> Trait for Inner<T>
where
    T: Debug + Send + 'static,
{
    #[inline]
    fn close(&mut self) {
        self.receiver.close();
    }

    #[inline]
    fn next(&mut self) -> FfiFuture<ROption<RSlice<'static, u8>>> {
        // we poll the future in UnboundedReceiver::poll_next
        unsafe { transmute(async move { self.next().await }.into_ffi()) }
    }
}

unsafe impl<T> Send for Receiver<T> where T: Send + StableAbi {}

impl<T> Stream for Receiver<T>
where
    T: Clone + Debug + Send + StableAbi + Unpin + 'static,
{
    type Item = T;

    #[inline]
    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let this = self.get_mut();

        let future = this.next.get_or_insert_with(|| this.inner.next());

        let pinned_future = pin!(future);

        match pinned_future.poll(cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(maybe_bytes) => {
                let _ = this.next.take();

                match maybe_bytes {
                    RNone => Poll::Ready(None),
                    RSome(bytes) => {
                        Poll::Ready(Some(
                            // we constructed this slice from an instance of T in Inner::next
                            unsafe { bytes.as_ptr().cast::<T>().as_ref() }
                                .expect("pointer must not be null")
                                .clone(),
                        ))
                    }
                }
            }
        }
    }
}

unsafe impl<T> Sync for Receiver<T> where T: Send + StableAbi {}

impl<T> Unpin for Receiver<T> where T: Debug + StableAbi {}
