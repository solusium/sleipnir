pub use crate::stream::{
    for_each_parallel::ForEachParallel,
    map_parallel_unordered::MapParallelUnordered,
    try_for_each_parallel::{Error as TryForEachParallelError, TryForEachParallel},
};
use {
    crate::runtime::Handle,
    core::{future::Future, num::NonZeroUsize},
    futures::{Stream, TryStream},
};

extern crate alloc;

mod for_each_parallel;
mod map_parallel_unordered;
mod try_for_each_parallel;

/// An extension trait for [`futures::Stream`] that provides a variety of convenient combinator
/// functions.
pub trait MoreStreamExt: Stream {
    /// Runs this stream to completion, executing the provided asynchronous closure for each
    /// elements on the stream, allowing the runtime to schedule the closure for one element
    /// in parallel to other elements if supported.
    ///
    /// This compares to [`futures::StreamExt::for_each_concurrent`], but the runtime might run the
    /// futures produced by the closure in parallel if the runtime operates on more then one thread.
    /// This combinator does not introduce any threads. On a single-threaded runtime this
    /// combinator behaves like `futures::StreamExt::for_each_concurrent`.
    ///
    /// We call the provided closure for each item this stream produces, yielding a future. We spawn
    /// a task awaiting this future, allowing the execution to completion in parallel with the other
    /// futures produced by the closure.
    ///
    /// The first argument describes an optional limit on the number of parallel futures. If this
    /// limit does not compare to None, we will run no more than limit futures in parallel. The
    /// limit argument has the type Into<Option<`usize`>>, and so we can provide it as either None,
    /// Some(10), or just 10. Note: we interpret a limit of zero as no limit at all, and will have
    /// the same result as passing in None.
    ///
    /// The second argument describes an optional handle to the runtime to spawn the futures on. If
    /// this handle does not compare to None, we will spawn the futures on this handle. Otherwise
    /// we retrieve the handle of the current runtime to spawn the futures.
    ///
    /// # Examples
    ///
    /// ```
    /// use {
    ///     futures::{channel::mpsc::channel, SinkExt, stream, StreamExt},
    ///     sleipnir::stream::MoreStreamExt,
    /// };
    ///
    /// # #[sleipnir::main]
    /// # async fn main(_handle: sleipnir::runtime::Handle) {
    ///
    /// let (sender_, receiver) = channel(5);
    ///
    /// {
    ///     let sender = sender_;
    ///     let sender_ref = &sender;
    ///
    ///     stream::iter([0, 1, 2, 3, 4])
    ///         .map(|element| (sender_ref.clone(), element))
    ///         .for_each_parallel(None, None, |(sender, element)| async move {
    ///             assert!(sender.clone().send(2 * element).await.is_ok());
    ///         })
    ///         .await;
    ///
    /// }
    ///
    /// let mut expected_results = vec![0, 2, 4, 6, 8];
    ///
    /// receiver
    ///     .collect::<Vec<_>>()
    ///     .await
    ///     .into_iter()
    ///     .for_each(|element| {
    ///         let maybe_index = expected_results
    ///             .iter()
    ///             .position(|element_| element == *element_);
    ///         assert!(maybe_index.is_some());
    ///         expected_results.remove(maybe_index.unwrap());
    ///     });
    ///
    /// assert!(expected_results.is_empty());
    /// # }
    /// ```
    #[inline]
    fn for_each_parallel<F, Fut>(
        self,
        limit: impl Into<Option<NonZeroUsize>>,
        handle: impl Into<Option<Handle>>,
        f: F,
    ) -> ForEachParallel<Self, F>
    where
        Self: Sized,
        F: FnMut(Self::Item) -> Fut,
        Fut: Future<Output = ()>,
    {
        ForEachParallel::new(self, f, limit.into(), handle.into())
    }
    /// Maps a stream like [`futures::StreamExt::map`] but polls the stream in parallel, yielding
    /// items in any order, as they made available.
    ///
    /// The first argument describes an optional limit on the number of parallel futures. If this
    /// limit does not compare to None, we will run no more than limit futures in parallel. The
    /// limit argument has the type Into<Option<`usize`>>, and so we can provide it as either None,
    /// Some(10), or just 10. Note: we interpret a limit of zero as no limit at all, and will have
    /// the same result as passing in None.
    ///
    /// Note that this function consumes the stream passed into it and returns a wrapped version of
    /// it.
    ///
    /// # Examples
    ///
    /// ```
    /// use {
    ///     futures::{stream, StreamExt},
    ///     sleipnir::stream::MoreStreamExt
    /// };
    ///
    /// # #[sleipnir::main]
    /// # async fn main(_handle: sleipnir::runtime::Handle) {
    ///
    /// let mut results = stream::iter([0u32, 1, 2, 3, 4])
    ///     .map_parallel_unordered(None, |element| async move { u64::from(element) * 2 })
    ///     .collect::<Vec<_>>()
    ///     .await
    ///     .into_iter()
    ///     .flatten()
    ///     .collect::<Vec<u64>>();
    ///
    /// [0u64, 2, 4, 6, 8].into_iter().for_each(|element| {
    ///     let maybe_index = results.iter().position(|element_| element == *element_);
    ///
    ///     assert!(maybe_index.is_some());
    ///
    ///     results.remove(maybe_index.unwrap());
    /// });
    ///
    /// assert!(results.is_empty());
    /// # }
    /// ```
    #[inline]
    fn map_parallel_unordered<F, Fut, T>(
        self,
        limit: impl Into<Option<NonZeroUsize>>,
        f: F,
    ) -> MapParallelUnordered<Self, F, T>
    where
        Self: Sized,
        F: FnMut(Self::Item) -> Fut,
        Fut: Future<Output = T>,
    {
        MapParallelUnordered::new(self, f, limit.into())
    }
}

/// Adapters specific to Result-returning streams.
pub trait MoreTryStreamExt: TryStream {
    /// Attempts to run this stream to completion, executing the provided asynchronous closure for
    /// each element on the stream in parallel as elements become available, exiting as soon as an
    /// error occurs.
    ///
    /// This compares to [`futures::TryStreamExt::try_for_each_concurrent`], but the runtime might
    /// run the futures produced by the closure in parallel if the runtime operates on more then one
    /// thread. This combinator does not introduce any threads. On a single-threaded runtime this
    /// combinator behaves like `futures::StreamExt::for_each_concurrent`.
    ///
    /// It will await any remaining futures and resolve to an error if the underlying stream or the
    /// provided closure return an error.
    ///
    /// # Examples
    ///
    /// ```
    /// use {
    ///     futures::{channel::mpsc::channel, SinkExt, stream, StreamExt},
    ///     sleipnir::{Error, stream::MoreTryStreamExt},
    /// };
    ///
    /// # #[sleipnir::main]
    /// # async fn main(_handle: sleipnir::runtime::Handle) {
    /// {
    ///     let result = stream::iter([Ok(0), Ok(1), Err(2), Ok(3), Ok(4)])
    ///         .try_for_each_parallel(None, |_| async move {
    ///             Ok(())
    ///         })
    ///         .await;
    ///
    ///     assert!(result.is_err());
    ///
    ///     assert!(
    ///         if let sleipnir::stream::TryForEachParallelError::Error(error) = result.unwrap_err() {
    ///             2 == error
    ///         } else {
    ///             false
    ///         }
    ///     );
    /// }
    /// {
    ///     let inputs: [Result<i32, ()>; 5] = [Ok(0), Ok(1), Ok(2), Ok(3), Ok(4)];
    ///
    ///     let result = stream::iter(inputs)
    ///         .try_for_each_parallel(None, |_| async move {
    ///             panic!();
    ///         })
    ///         .await;
    ///
    ///     assert!(result.is_err());
    ///     
    ///     assert!(
    ///         if let sleipnir::stream::TryForEachParallelError::TaskError(error) = result.unwrap_err() {
    ///             Error::TaskPanic == error
    ///         } else {
    ///             false
    ///         }
    ///     );
    /// }
    /// # }
    /// ```
    #[inline]
    fn try_for_each_parallel<Fut, F>(
        self,
        limit: impl Into<Option<usize>>,
        f: F,
    ) -> TryForEachParallel<Self, F>
    where
        Self: Sized,
        Self::Ok: Send + 'static,
        Self::Error: Send + 'static,
        F: FnMut(Self::Ok) -> Fut + Clone + Send + Sync + 'static,
        Fut: Future<Output = Result<(), Self::Error>> + Send,
    {
        TryForEachParallel::new(self, limit.into(), f)
    }
}

impl<S: ?Sized + Stream> MoreStreamExt for S {}

impl<S: ?Sized + TryStream> MoreTryStreamExt for S {}
