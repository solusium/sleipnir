#[cfg(feature = "ffi")]
use abi_stable::StableAbi;
use core::fmt::{Debug, Display, Formatter, Result};

/// Errors which functions in this crate might return.
#[derive(Clone, Eq, PartialEq)]
#[cfg_attr(feature = "ffi", repr(C), derive(StableAbi))]
pub enum Error {
    /// When awaiting a cancelled task via its [`JoinHandle`](crate::task::JoinHandle), this will
    /// return an Err([`Error::Cancelled`]).
    Cancelled,
    /// When calling [`Handle::try_current`](crate::runtime::Handle::try_current) from outside the
    /// context of a runtime, this will return an Err([`Error::NoContext`]),
    NoContext,
    /// When awaiting a panicked task via its [`JoinHandle`](crate::task::JoinHandle), this will
    /// return an Err([`Error::TaskPanic`]).
    TaskPanic,
    /// When calling [`Handle::try_current`](crate::runtime::Handle::try_current) while we
    /// dropped the context thread-local dropped, this will return an
    /// Err([`Error::ThreadLocalDestroyed`]).
    ThreadLocalDestroyed,
}

impl Debug for Error {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        match self {
            Self::Cancelled => f.write_str("the task was cancelled"),
            Self::NoContext => f.write_str(
                "there is no runtime running, must be called from the context of a runtime",
            ),
            Self::TaskPanic => f.write_str("the task panicked"),
            Self::ThreadLocalDestroyed => {
                f.write_str("the context thread-local variable has been destroyed")
            }
        }
    }
}

impl Display for Error {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        Debug::fmt(self, f)
    }
}

unsafe impl Send for Error {}

unsafe impl Sync for Error {}
