use {
    crate::{runtime::Handle, task::JoinHandle, Error},
    alloc::vec::Vec,
    core::{
        fmt::{Debug, Formatter},
        future::Future,
        num::NonZeroUsize,
        pin::Pin,
        task::{Context, Poll},
    },
    futures::stream::{FusedStream, Stream},
    pin_project_lite::pin_project,
};

extern crate alloc;

pin_project! {
    /// Stream for the [`map_parallel_unordered`](crate::stream::StreamExt::map_parallel_unordered)
    /// method.
    pub struct MapParallelUnordered<S, F, T> {
        #[pin]
        stream: Option<S>,
        f: F,
        tasks: Vec<JoinHandle<T>>,
        limit: Option<NonZeroUsize>,
    }
}

impl<S, F, T> MapParallelUnordered<S, F, T> {
    #[inline]
    pub(super) const fn new(stream: S, f: F, limit: Option<NonZeroUsize>) -> Self {
        let stream = Some(stream);
        let tasks = Vec::new();
        Self {
            stream,
            f,
            tasks,
            limit,
        }
    }
}

impl<S, F, T> Debug for MapParallelUnordered<S, F, T>
where
    S: Debug,
    T: Debug,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("MapParallelUnordered")
            .field("stream", &self.stream)
            .field("tasks", &self.tasks)
            .field("limit", &self.limit)
            .finish_non_exhaustive()
    }
}

impl<S, F, Fut, T> FusedStream for MapParallelUnordered<S, F, T>
where
    S: Stream,
    S::Item: Clone + Send + 'static,
    F: FnMut(S::Item) -> Fut + Clone + Send + Sync + 'static,
    Fut: Future<Output = T> + Send,
    T: Send + 'static,
{
    #[inline]
    fn is_terminated(&self) -> bool {
        self.stream.is_none() && self.tasks.is_empty()
    }
}

unsafe impl<S, F, T> Send for MapParallelUnordered<S, F, T>
where
    S: Send,
    F: Send,
    T: Send,
{
}

impl<S, F, Fut, T> Stream for MapParallelUnordered<S, F, T>
where
    S: Stream,
    S::Item: Clone + Send + 'static,
    F: FnMut(S::Item) -> Fut + Clone + Send + Sync + 'static,
    Fut: Future<Output = T> + Send,
    T: Send + 'static,
{
    type Item = Result<T, Error>;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let mut this = self.project();

        loop {
            let mut made_progress_this_iter = false;

            let can_spawn_another_task = this
                .limit
                .map_or(true, |limit| limit.get() > this.tasks.len());

            if can_spawn_another_task {
                let mut poll_next_element = || {
                    let mut stream_completed_ = false;

                    let maybe_element_ = this.stream.as_mut().as_pin_mut().and_then(|stream| {
                        match stream.poll_next(cx) {
                            Poll::Pending => None,
                            Poll::Ready(None) => {
                                stream_completed_ = true;
                                None
                            }
                            Poll::Ready(Some(element)) => {
                                made_progress_this_iter = true;
                                Some(element)
                            }
                        }
                    });

                    (stream_completed_, maybe_element_)
                };

                let (stream_completed, maybe_element) = poll_next_element();

                if stream_completed {
                    this.stream.set(None);
                }

                if let Some(element) = maybe_element {
                    let mut f = this.f.clone();

                    this.tasks
                        .push(Handle::current().spawn(async move { (f)(element).await }));
                }
            }

            if this.tasks.is_empty() && this.stream.is_none() {
                return Poll::Ready(None);
            }

            let maybe_index_and_result =
                this.tasks.iter_mut().enumerate().find_map(|(index, task)| {
                    crate::pin!(task);

                    match task.poll(cx) {
                        Poll::Pending => None,
                        Poll::Ready(result) => Some((index, result)),
                    }
                });

            if let Some((index, result)) = maybe_index_and_result {
                drop(this.tasks.remove(index));

                return Poll::Ready(Some(result));
            }

            if !made_progress_this_iter {
                return Poll::Pending;
            }
        }
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.stream
            .as_ref()
            .map_or((0, None), futures::Stream::size_hint)
    }
}
