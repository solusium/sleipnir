use {
    crate::{pin, runtime::Handle, task::JoinHandle},
    alloc::vec::Vec,
    core::{
        future::Future,
        num::NonZeroUsize,
        pin::Pin,
        task::{Context, Poll},
    },
    futures::{future::FusedFuture, TryStream},
    pin_project_lite::pin_project,
};

extern crate alloc;

/// Error which [`TryForEachParallel`] might return when awaited.
pub enum Error<E> {
    /// Returns this error when a task executing the closure returns an error when awaited, e.g.
    /// when the closure panicked.
    TaskError(crate::Error),
    /// Returns this error when the closure returns an error when awaited.
    Error(E),
}

pin_project! {
    /// Future for the [`try_for_each_parallel`](crate::stream::TryStream::try_for_each_parallel)
    /// method.
    #[must_use = "futures do nothing unless you `.await` or poll them"]
    pub struct TryForEachParallel<S, F>
    where
        S: TryStream,

    {
        #[pin]
        stream: Option<S>,
        f: F,
        tasks: Vec<JoinHandle<Result<(), S::Error>>>,
        limit: Option<NonZeroUsize>,
        error: Option<Error<S::Error>>,
    }
}

impl<S, Fut, F> TryForEachParallel<S, F>
where
    S: TryStream,
    S::Ok: Send + 'static,
    S::Error: Send + 'static,
    F: FnMut(S::Ok) -> Fut + Clone + Send + Sync + 'static,
    Fut: Future<Output = Result<(), S::Error>> + Send,
{
    #[inline]
    pub(super) fn new(stream: S, limit: Option<usize>, f: F) -> Self {
        let stream = Some(stream);
        let limit = limit.and_then(NonZeroUsize::new);
        let tasks = Vec::new();
        let error = None;

        Self {
            stream,
            f,
            tasks,
            limit,
            error,
        }
    }

    #[inline]
    fn on_first_error(
        error: Error<S::Error>,
        stream: &mut Pin<&mut Option<S>>,
        tasks: &mut [JoinHandle<Result<(), S::Error>>],
        cx: &mut Context<'_>,
        this_error: &mut Option<Error<S::Error>>,
    ) -> Poll<<Self as Future>::Output> {
        stream.set(None);

        if Self::tasks_are_ready(tasks, cx) {
            Poll::Ready(Err(error))
        } else {
            *this_error = Some(error);

            Poll::Pending
        }
    }

    #[inline]
    fn maybe_process_next_stream_value(
        limit: Option<NonZeroUsize>,
        tasks: &mut Vec<JoinHandle<Result<(), S::Error>>>,
        stream: &mut Pin<&mut Option<S>>,
        cx: &mut Context<'_>,
        mut f: F,
        this_error: &mut Option<Error<S::Error>>,
    ) -> Option<Poll<<Self as Future>::Output>> {
        let can_spawn_another_task = limit.map_or(true, |limit_| limit_.get() > tasks.len());

        if !can_spawn_another_task {
            None
        } else {
            let poll_result = stream
                .as_mut()
                .as_pin_mut()
                .map_or_else(|| Poll::Ready(None), |stream| stream.try_poll_next(cx));

            let (maybe_element, maybe_poll_result) = match poll_result {
                Poll::Pending => (None, None),
                Poll::Ready(None) => {
                    stream.set(None);

                    (None, None)
                }
                Poll::Ready(Some(Ok(element_))) => (Some(element_), None),
                Poll::Ready(Some(Err(error))) => (
                    None,
                    Some(Self::on_first_error(
                        Error::Error(error),
                        stream,
                        tasks,
                        cx,
                        this_error,
                    )),
                ),
            };

            if maybe_poll_result.is_some() {
                maybe_poll_result
            } else {
                if let Some(element) = maybe_element {
                    tasks.push(Handle::current().spawn(async move { f(element).await }));
                }

                None
            }
        }
    }

    #[inline]
    fn process_tasks(
        tasks: &mut Vec<JoinHandle<Result<(), S::Error>>>,
        stream: &mut Pin<&mut Option<S>>,
        cx: &mut Context<'_>,
        this_error: &mut Option<Error<S::Error>>,
    ) -> Option<Poll<<Self as Future>::Output>> {
        if tasks.is_empty() && stream.is_none() {
            Some(Poll::Ready(Ok(())))
        } else {
            let maybe_index_and_result = tasks.iter_mut().enumerate().find_map(|(index, task)| {
                crate::pin!(task);

                match task.poll(cx) {
                    Poll::Pending => None,
                    Poll::Ready(result) => Some((index, result)),
                }
            });

            match maybe_index_and_result {
                None => Some(Poll::Pending),
                Some((index, task_result)) => {
                    drop(tasks.remove(index));

                    match task_result {
                        Ok(result) => match result {
                            Ok(()) => None,
                            Err(error) => Some(Self::on_first_error(
                                Error::Error(error),
                                stream,
                                tasks,
                                cx,
                                this_error,
                            )),
                        },
                        Err(error) => Some(Self::on_first_error(
                            Error::TaskError(error),
                            stream,
                            tasks,
                            cx,
                            this_error,
                        )),
                    }
                }
            }
        }
    }

    #[inline]
    fn tasks_are_ready(
        tasks: &mut [JoinHandle<Result<(), S::Error>>],
        cx: &mut Context<'_>,
    ) -> bool {
        tasks.iter_mut().all(|task| {
            pin!(task);
            task.poll(cx).is_ready()
        })
    }
}

impl<S, Fut, F> FusedFuture for TryForEachParallel<S, F>
where
    S: TryStream,
    S::Ok: Send + 'static,
    S::Error: Send + 'static,
    F: FnMut(S::Ok) -> Fut + Clone + Send + Sync + 'static,
    Fut: Future<Output = Result<(), S::Error>> + Send,
{
    #[inline]
    fn is_terminated(&self) -> bool {
        self.stream.is_none() && self.tasks.is_empty()
    }
}

impl<S, Fut, F> Future for TryForEachParallel<S, F>
where
    S: TryStream,
    S::Ok: Send + 'static,
    S::Error: Send + 'static,
    F: FnMut(S::Ok) -> Fut + Clone + Send + Sync + 'static,
    Fut: Future<Output = Result<(), S::Error>> + Send,
{
    type Output = Result<(), Error<S::Error>>;

    #[inline]
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut this = self.project();

        if let Some(error) = this.error.take() {
            if Self::tasks_are_ready(this.tasks, cx) {
                return Poll::Ready(Err(error));
            }

            *this.error = Some(error);

            return Poll::Pending;
        }

        loop {
            let maybe_proccess_stream_value_poll_result = Self::maybe_process_next_stream_value(
                *this.limit,
                this.tasks,
                &mut this.stream,
                cx,
                this.f.clone(),
                this.error,
            );

            if let Some(proccess_stream_value_poll_result) = maybe_proccess_stream_value_poll_result
            {
                return proccess_stream_value_poll_result;
            }

            let maybe_process_tasks_poll_result =
                Self::process_tasks(this.tasks, &mut this.stream, cx, this.error);

            if let Some(procces_tasks_poll_result) = maybe_process_tasks_poll_result {
                return procces_tasks_poll_result;
            }
        }
    }
}
