use {
    crate::{runtime::Handle, task::JoinHandle},
    core::{
        fmt::{Debug, Formatter},
        future::Future,
        num::NonZeroUsize,
        pin::Pin,
        task::{Context, Poll},
    },
    futures::{future::FusedFuture, Stream},
    pin_project_lite::pin_project,
};

pin_project! {
    /// Future for the [`for_each_concurrent`](crate::stream::StreamExt::for_each_parallel)
    /// method.
    pub struct ForEachParallel<S, F> {
        #[pin]
        stream: Option<S>,
        f: F,
        tasks: Vec<JoinHandle<()>>,
        limit: Option<NonZeroUsize>,
        handle: Handle,
    }
}

impl<S, F> ForEachParallel<S, F> {
    #[inline]
    pub(super) fn new(
        stream: S,
        f: F,
        limit: Option<NonZeroUsize>,
        handle: Option<Handle>,
    ) -> Self {
        let stream = Some(stream);
        let tasks = Vec::new();
        let handle = handle.unwrap_or_else(Handle::current);

        Self {
            stream,
            f,
            tasks,
            limit,
            handle,
        }
    }
}

impl<S, F> Debug for ForEachParallel<S, F>
where
    S: Debug,
{
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("ForEachParallel")
            .field("stream", &self.stream)
            .field("tasks", &self.tasks)
            .field("limit", &self.limit)
            .finish_non_exhaustive()
    }
}

impl<S, F, Fut> Future for ForEachParallel<S, F>
where
    S: Stream,
    S::Item: Clone + Send + 'static,
    F: FnMut(S::Item) -> Fut + Clone + Send + Sync + 'static,
    Fut: Future<Output = ()> + Send,
{
    type Output = ();

    #[inline]
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut this = self.project();

        loop {
            let mut made_progress_this_iter = false;

            if this
                .limit
                .map_or(true, |limit| limit.get() > this.tasks.len())
            {
                let mut stream_completed = false;

                let maybe_element =
                    this.stream.as_mut().as_pin_mut().and_then(|stream| {
                        match stream.poll_next(cx) {
                            Poll::Pending => None,
                            Poll::Ready(None) => {
                                stream_completed = true;
                                None
                            }
                            Poll::Ready(Some(element)) => {
                                made_progress_this_iter = true;
                                Some(element)
                            }
                        }
                    });

                if stream_completed {
                    this.stream.set(None);
                }

                if let Some(element) = maybe_element {
                    let mut f = this.f.clone();

                    this.tasks.push(this.handle.spawn(async move {
                        (f)(element).await;
                    }));
                }
            }

            if this.tasks.is_empty() && this.stream.is_none() {
                return Poll::Ready(());
            }

            let old_length = this.tasks.len();

            this.tasks.retain_mut(|task| {
                crate::pin!(task);

                task.poll(cx).is_pending()
            });

            if old_length != this.tasks.len() {
                made_progress_this_iter = true;
            }

            if !made_progress_this_iter {
                return Poll::Pending;
            }
        }
    }
}

impl<S, F, Fut> FusedFuture for ForEachParallel<S, F>
where
    S: Stream,
    S::Item: Clone + Send + 'static,
    F: FnMut(S::Item) -> Fut + Clone + Send + Sync + 'static,
    Fut: Future<Output = ()> + Send,
{
    #[inline]
    fn is_terminated(&self) -> bool {
        self.stream.is_none() && self.tasks.is_empty()
    }
}

unsafe impl<S, F> Send for ForEachParallel<S, F>
where
    S: Send,
    F: Send,
{
}
