#![warn(clippy::all, clippy::pedantic, clippy::nursery, clippy::cargo)]
#![allow(
    clippy::explicit_deref_methods,
    clippy::if_not_else,
    clippy::multiple_crate_versions,
    clippy::redundant_pub_crate,
    clippy::type_complexity
)]

#[allow(unused_imports)]
use crate::{
    runtime::{Handle, Runtime},
    task::{yield_now, JoinHandle},
};

/// FFI-safe wrappers for [`Handle`], [`JoinHandle`] and [`core::task::Waker`].
pub mod ffi;
/// Allows to construct a runtime via [`Runtime`] and spawn task on a runtime via a [`Handle`].
pub mod runtime;
/// Asynchronous streams.
pub mod stream;
/// Offers a handle to task via [`JoinHandle`], allowing to join a task, and [`yield_now`] to yield
/// execution to the runtime.
pub mod task;

pub(crate) mod error;

mod macros;

pub use crate::{error::Error, macros::*};
#[cfg(feature = "macros")]
pub use sleipnir_macros::*;
