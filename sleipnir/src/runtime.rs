use {
    crate::{task::JoinHandle, Error},
    core::future::Future,
    tokio::runtime::Builder,
};

/// Handle to the runtime.
///
/// See [`tokio::runtime::Handle`] for more information.
#[derive(Clone, Debug)]
pub struct Handle {
    implementation: tokio::runtime::Handle,
}

/// The runtime constructed via [`Runtime::new`]. It provides a task scheduler necessary for
/// running asynchronous tasks.
///
/// See [`tokio::runtime::Runtime`] for more information.
#[derive(Debug)]
pub struct Runtime {
    implementation: tokio::runtime::Runtime,
}

impl Handle {
    /// Returns a handle to the current runtime.
    ///
    /// # Panics
    ///
    /// This will panic outside the context of a runtime.
    ///
    /// See [`tokio::runtime::Handle::current`] for more information.
    #[inline]
    #[must_use]
    pub fn current() -> Self {
        Self {
            implementation: tokio::runtime::Handle::current(),
        }
    }

    /// Spawns a future onto the runtime.
    ///
    /// The provided future will start running in the background when calling spawn,
    /// even if you don’t await the returned [`JoinHandle`].
    ///
    /// See [`tokio::runtime::Handle::spawn`] for more information.
    #[inline]
    pub fn spawn<F>(&self, future: F) -> JoinHandle<F::Output>
    where
        F: Future + Send + 'static,
        F::Output: Send + 'static,
    {
        self.implementation.spawn(future).into()
    }

    /// Returns a handle to the running runtime.
    ///
    /// # Errors
    ///
    /// This function will return an error if not inside the context of a runtime
    /// [`Error::NoContext`] since we need to call this function from the context of a runtime.
    /// Another error might occur if we destroyed the context thread-local variable
    /// [`Error::ThreadLocalDestroyed`]. This can happen when dropping other thread-locals.
    ///
    /// See [`tokio::runtime::Handle::try_current`] for more information.
    #[inline]
    pub fn try_current() -> Result<Self, Error> {
        Ok(Self {
            implementation: match tokio::runtime::Handle::try_current() {
                Ok(implementation_) => Ok(implementation_),
                Err(error) => {
                    if error.is_missing_context() {
                        Err(Error::NoContext)
                    } else {
                        Err(Error::ThreadLocalDestroyed)
                    }
                }
            }?,
        })
    }
}

impl Runtime {
    /// Constructs a runtime.
    ///
    /// # Errors
    ///
    /// This function will return an error if the builder of the underlying implementation returns
    /// an error.
    #[inline]
    pub fn new() -> std::io::Result<Self> {
        match Builder::new_multi_thread().build() {
            Ok(implementation) => Ok(Self { implementation }),
            Err(error) => Err(error),
        }
    }

    /// The entry point of the runtime. Runs the future to completion.
    ///
    /// This runs the given future on the current thread, blocking until completing it, and
    /// yielding its resolved result. Executes tasks spawned by the future on the runtime.
    #[inline]
    pub fn run<F>(&'static mut self, future: F)
    where
        F: Future,
    {
        self.implementation.block_on(future);
    }
}

impl From<tokio::runtime::Handle> for Handle {
    #[inline]
    fn from(implementation: tokio::runtime::Handle) -> Self {
        Self { implementation }
    }
}

#[allow(clippy::from_over_into)]
impl Into<tokio::runtime::Handle> for Handle {
    #[inline]
    fn into(self) -> tokio::runtime::Handle {
        self.implementation
    }
}

unsafe impl Send for Handle {}

unsafe impl Sync for Handle {}

impl From<tokio::runtime::Runtime> for Runtime {
    #[inline]
    fn from(implementation: tokio::runtime::Runtime) -> Self {
        Self { implementation }
    }
}

#[allow(clippy::from_over_into)]
impl Into<tokio::runtime::Runtime> for Runtime {
    #[inline]
    fn into(self) -> tokio::runtime::Runtime {
        self.implementation
    }
}

unsafe impl Send for Runtime {}

unsafe impl Sync for Runtime {}
