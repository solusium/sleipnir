use {
    crate::Error,
    core::{
        future::Future,
        pin::Pin,
        task::{Context, Poll},
    },
};

/// Yield execution back to the runtime.
///
/// See [`tokio::task::yield_now`] for more information.
#[inline]
pub async fn yield_now() {
    tokio::task::yield_now().await;
}

/// An owning handle to a task, constructed by [`Handle::spawn`](crate::Handle::spawn) and used to
/// join a task.
///
/// [`JoinHandle`] detaches the associated task when we drop it, which means we won't have any
/// handle to the task any longer, and no way to join on it.
///
/// See [`tokio::task::JoinHandle`] for more information.
#[derive(Debug)]
pub struct JoinHandle<T> {
    implementation: tokio::task::JoinHandle<T>,
}

impl<T> From<tokio::task::JoinHandle<T>> for JoinHandle<T> {
    fn from(implementation: tokio::task::JoinHandle<T>) -> Self {
        Self { implementation }
    }
}

impl<T> Future for JoinHandle<T> {
    type Output = Result<T, Error>;

    #[inline]
    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        match Pin::new(&mut self.implementation).poll(cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(result) => match result {
                Ok(value) => Poll::Ready(Ok(value)),
                Err(error) => {
                    if error.is_cancelled() {
                        unreachable!("Cancellation of tasks should not be exported by sleipnir.")
                    } else {
                        assert!(error.is_panic());

                        Poll::Ready(Err(Error::TaskPanic))
                    }
                }
            },
        }
    }
}

#[allow(clippy::from_over_into)]
impl<T> Into<tokio::task::JoinHandle<T>> for JoinHandle<T> {
    fn into(self) -> tokio::task::JoinHandle<T> {
        self.implementation
    }
}
