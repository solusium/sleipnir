use {
    async_ffi::FutureExt,
    oneshot::channel,
    sleipnir::{ffi, runtime::Handle},
    sleipnir_test_abi_stable_module_declaration::tests::{current, spawn},
    std::{env::var, path::Path},
};

mod sync;

#[sleipnir::test]
async fn no_return_value(handle: Handle) {
    let ffi_handle = ffi::runtime::Handle::new(handle);

    let (sender, receiver) = channel();

    let join_handle = ffi_handle.spawn(async move {
        assert!(sender.send(42).is_ok());
    });

    let task_result = join_handle.await;

    assert!(task_result.is_ok());

    let result = receiver.await;

    assert!(result.is_ok());

    assert_eq!(42, result.unwrap());
}

#[sleipnir::test]
async fn with_return_value(handle: Handle) {
    let ffi_handle = ffi::runtime::Handle::new(handle);

    let join_handle = ffi_handle.spawn(async move { 42 });

    let task_result = join_handle.await;

    assert!(task_result.is_ok());

    assert_eq!(42, task_result.unwrap());
}

#[sleipnir::test]
async fn not_joining_still_executing(handle: Handle) {
    let ffi_handle = ffi::runtime::Handle::new(handle);

    let (sender, receiver) = channel();

    {
        let _join_handle = ffi_handle.spawn(async move {
            assert!(sender.send(42).is_ok());
        });
    }

    let receiv_result = receiver.await;

    assert!(receiv_result.is_ok());

    assert_eq!(42, receiv_result.unwrap());
}

#[sleipnir::test]
async fn current_over_ffi_boundry(_handle: Handle) {
    let library_path_string_result = var("SLEIPNIR_TEST_ABI_STABLE_MODULE_LIBRARY_PATH");

    assert!(library_path_string_result.is_ok());

    let library_path_string = library_path_string_result.unwrap();

    let path = Path::new(&library_path_string);

    let result = current(path);

    assert!(result.is_err());
}

#[sleipnir::test]
async fn spawning_over_ffi_boundry(handle: Handle) {
    let library_path_string_result = var("SLEIPNIR_TEST_ABI_STABLE_MODULE_LIBRARY_PATH");

    assert!(library_path_string_result.is_ok());

    let library_path_string = library_path_string_result.unwrap();

    let path = Path::new(&library_path_string);

    let future = async move { 42 }.into_ffi();

    let join_handle = spawn(path, handle.into(), future);

    let result = join_handle.await;

    assert!(result.is_ok());

    let value = result.unwrap();

    assert_eq!(42, value);
}
