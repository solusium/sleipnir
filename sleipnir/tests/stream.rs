use {
    async_lock::RwLock,
    futures::{channel::mpsc::channel, stream, SinkExt, StreamExt},
    rand::{distributions::Uniform, thread_rng, Rng},
    sleipnir::{
        runtime::Handle,
        stream::{MoreStreamExt, MoreTryStreamExt},
        Error,
    },
    std::{convert::Infallible, sync::Arc},
};

#[sleipnir::test]
async fn for_each_parallel_hello_world(_handle: Handle) {
    let simple_array = [42];

    let (sender, mut receiver) = channel(1);

    {
        let sender_ = sender;

        let sender_ref = &sender_;

        stream::iter(simple_array)
            .map(|element| (sender_ref.clone(), element))
            .for_each_parallel(None, None, |(mut sender, element)| async move {
                assert!(sender.send(element).await.is_ok());
            })
            .await;
    }

    assert_eq!(Some(simple_array[0]), receiver.next().await);
    assert!(receiver.next().await.is_none());
}

#[sleipnir::test]
async fn for_each_parallel_empty_stream(_handle: Handle) {
    let empty_array: [usize; 0] = [];

    let (sender, mut receiver) = channel(1);

    {
        let sender_ = sender;

        let sender_ref = &sender_;

        stream::iter(empty_array)
            .map(|element| (sender_ref.clone(), element))
            .for_each_parallel(None, None, |(sender, element)| async move {
                assert!(sender.clone().send(element).await.is_ok());
            })
            .await;
    }

    assert!(receiver.next().await.is_none());
}

#[sleipnir::test]
async fn for_each_parallel_random_stream(_handle: Handle) {
    let mut rng = thread_rng();

    let range = Uniform::from(0..usize::MAX);

    let random_vector: Vec<usize> = rng
        .clone()
        .sample_iter(&range)
        .take(2usize.pow(rng.gen_range(0..16)))
        .collect();

    let (sender, receiver) = channel(random_vector.len());

    {
        let sender_ = sender;

        let sender_ref = &sender_;

        stream::iter(random_vector.clone().into_iter())
            .map(|element| (sender_ref.clone(), element))
            .for_each_parallel(None, None, |(sender, element)| async move {
                assert!(sender.clone().send(element).await.is_ok());
            })
            .await;
    }

    let random_vector_arc = Arc::new(RwLock::new(random_vector));

    let random_vector_arc_ref = &random_vector_arc;

    receiver
        .for_each(|element| async move {
            let random_vector_arc_ = random_vector_arc_ref.clone();

            let mut random_vector_ = random_vector_arc_.write().await;

            let maybe_index = random_vector_
                .iter()
                .enumerate()
                .find(|(_, &element_)| element == element_);

            assert!(maybe_index.is_some());

            let (index, _) = maybe_index.unwrap();

            random_vector_.remove(index);
        })
        .await;

    assert!(random_vector_arc.read().await.is_empty());
}

fn map_closure(element: u32) -> u64 {
    u64::from(element) * 2
}

#[sleipnir::test]
async fn map_parallel_unordered_hello_world(_handle: Handle) {
    let simple_array: [u32; 1] = [42];

    let results = stream::iter(simple_array)
        .map_parallel_unordered(None, |element| async move { map_closure(element) })
        .collect::<Vec<_>>()
        .await
        .into_iter()
        .flatten()
        .collect::<Vec<u64>>();

    assert_eq!(1, results.len());
    assert_eq!(map_closure(simple_array[0]), results[0]);
}

#[sleipnir::test]
async fn map_parallel_unordered_empty_stream(_handle: Handle) {
    let empty_array: [u16; 0] = [];

    let results = stream::iter(empty_array)
        .map_parallel_unordered(None, |element| async move { f32::from(element) })
        .collect::<Vec<_>>()
        .await;

    assert!(results.is_empty());
}

#[sleipnir::test]
async fn map_parallel_unordered_random_stream(_handle: Handle) {
    let mut rng = thread_rng();

    let range = Uniform::from(0..u32::MAX);

    let random_vector: Vec<u32> = rng
        .clone()
        .sample_iter(&range)
        .take(2usize.pow(rng.gen_range(0..14)))
        .collect();

    let mut results = stream::iter(random_vector.clone().into_iter())
        .map_parallel_unordered(None, |element| async move { map_closure(element) })
        .collect::<Vec<_>>()
        .await
        .into_iter()
        .flatten()
        .collect::<Vec<u64>>();

    assert_eq!(random_vector.len(), results.len());

    random_vector.into_iter().for_each(|element| {
        let maybe_index = results
            .iter()
            .position(|element_| map_closure(element) == *element_);

        assert!(maybe_index.is_some());

        results.remove(maybe_index.unwrap());
    });

    assert!(results.is_empty());
}

#[sleipnir::test]
async fn try_for_each_parallel_hello_world(_handle: Handle) {
    let simple_array: [u32; 1] = [42];

    let (sender_, mut receiver) = channel(1);

    {
        let mut sender = sender_;

        let sender_ref = &mut sender;

        let result = stream::iter(simple_array)
            .map(|element| Ok((sender_ref.clone(), element)))
            .try_for_each_parallel(None, |(sender_, element)| async move {
                sender_.clone().send(element).await
            })
            .await;

        assert!(result.is_ok())
    }

    assert_eq!(Some(simple_array[0]), receiver.next().await);
    assert!(receiver.next().await.is_none());
}

#[sleipnir::test]
async fn try_for_each_parallel_empty_stream(_handle: Handle) {
    let empty_array: [Result<u16, Infallible>; 0] = [];

    let result = stream::iter(empty_array)
        .try_for_each_parallel(None, |element| async move {
            match f32::try_from(element) {
                Ok(_) => Ok(()),
                Err(error) => Err(error),
            }
        })
        .await;

    assert!(result.is_ok());
}

#[sleipnir::test]
async fn try_for_each_parallel_random_stream(_handle: Handle) {
    let mut rng = thread_rng();

    let range = Uniform::from(0..usize::MAX);

    let random_vector: Vec<usize> = rng
        .clone()
        .sample_iter(&range)
        .take(2usize.pow(rng.gen_range(0..16)))
        .collect();

    let (sender, receiver) = channel(random_vector.len());

    {
        let sender_ = sender;

        let sender_ref = &sender_;

        let result = stream::iter(random_vector.clone().into_iter())
            .map(|element| Ok((sender_ref.clone(), element)))
            .try_for_each_parallel(None, |(sender, element)| async move {
                sender.clone().send(element).await
            })
            .await;

        assert!(result.is_ok())
    }

    let random_vector_arc = Arc::new(RwLock::new(random_vector));

    let random_vector_arc_ref = &random_vector_arc;

    receiver
        .for_each(|element| async move {
            let random_vector_arc_ = random_vector_arc_ref.clone();

            let mut random_vector_ = random_vector_arc_.write().await;

            let maybe_index = random_vector_
                .iter()
                .enumerate()
                .find(|(_, &element_)| element == element_);

            assert!(maybe_index.is_some());

            let (index, _) = maybe_index.unwrap();

            random_vector_.remove(index);
        })
        .await;

    assert!(random_vector_arc.read().await.is_empty());
}

#[sleipnir::test]
async fn try_for_each_parallel_erroneous_stream(_handle: Handle) {
    let input = [Ok(0), Err(1), Ok(2)];

    let result = stream::iter(input)
        .try_for_each_parallel(None, |_| async move { Ok(()) })
        .await;

    assert!(result.is_err());

    assert!(
        if let sleipnir::stream::TryForEachParallelError::Error(error) = result.unwrap_err() {
            1 == error
        } else {
            false
        }
    );
}

#[sleipnir::test]
async fn try_for_each_parallel_panicing_closure(_handle: Handle) {
    let input: [Result<u32, u32>; 3] = [Ok(0), Ok(1), Ok(2)];

    let result = stream::iter(input)
        .try_for_each_parallel(None, |_| async move {
            panic!();
        })
        .await;

    assert!(result.is_err());

    assert!(match result.unwrap_err() {
        sleipnir::stream::TryForEachParallelError::TaskError(error) => {
            Error::TaskPanic == error
        }
        sleipnir::stream::TryForEachParallelError::Error(_) => false,
    });
}
