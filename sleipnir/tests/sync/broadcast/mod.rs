use {
    abi_stable::StableAbi,
    async_lock::Mutex,
    futures::{stream, StreamExt},
    rand::{thread_rng, Rng},
    sleipnir::{
        ffi::sync::{
            broadcast::{channel, Receiver},
            Error,
        },
        runtime::Handle,
        stream::MoreStreamExt,
    },
    std::sync::Arc,
};

#[repr(C)]
#[derive(Clone, Debug, Eq, PartialEq, StableAbi)]
enum Message {
    HelloWorld,
    WithContent(usize),
}

#[sleipnir::test]
async fn hello_world(_handle: Handle) {
    let (sender, mut receiver) = channel(1);

    {
        let send_result = sender.send(Message::HelloWorld);

        assert!(send_result.is_ok());
    }

    let maybe_message = receiver.next().await;

    assert!(maybe_message.is_some());

    assert_eq!(Message::HelloWorld, maybe_message.unwrap());
}

#[sleipnir::test]
async fn closed_counterpart(_handle: Handle) {
    {
        let (sender, _) = channel(1);

        let send_result = sender.send(Message::HelloWorld);

        assert!(send_result.is_err());

        assert_eq!(Error::Disconnected, send_result.unwrap_err());
    }

    let (_, mut receiver) = channel::<Message>(1);

    let maybe_message = receiver.next().await;

    assert!(maybe_message.is_none());
}

#[sleipnir::test]
async fn multiple_sender_receiver_and_messages(_handle: Handle) {
    let values = (0..thread_rng().gen_range(2..=8))
        .map(|_| {
            (0..thread_rng().gen_range(2..=8))
                .map(|_| thread_rng().gen::<usize>())
                .collect::<Vec<usize>>()
        })
        .collect::<Vec<Vec<usize>>>();

    let (sender, receiver) = channel(values.iter().map(|sub_values| sub_values.len()).sum());

    let receivers = (0..thread_rng().gen_range(2..8))
        .map(|index| {
            if 0 == (index % 2) {
                sender.subscribe()
            } else {
                receiver.resubscribe()
            }
        })
        .collect::<Vec<Receiver<Message>>>();

    {
        let sender_ref = &sender;

        stream::iter(values.clone().into_iter())
            .map(|sub_values| (sender_ref.clone(), sub_values.clone()))
            .for_each_parallel(None, None, |(sender, sub_values)| async move {
                for value in sub_values {
                    assert!(sender.send(Message::WithContent(value)).is_ok());
                }
            })
            .await;
    }

    drop(sender);

    stream::iter(
        receivers
            .into_iter()
            .map(|receiver| (receiver, Arc::new(Mutex::new(values.clone())))),
    )
    .for_each(|(receiver, values)| async move {
        let values_ref = &values;

        receiver
            .for_each(|message| async move {
                if let Message::WithContent(value) = message {
                    let values_clone = values_ref.clone();

                    let mut values_lock = values_clone.lock().await;

                    let maybe_index = values_lock
                        .iter()
                        .position(|sub_values| sub_values.contains(&value));

                    assert!(maybe_index.is_some());

                    let sub_values = &mut values_lock[maybe_index.unwrap()];

                    let maybe_sub_index = sub_values
                        .iter()
                        .position(|possible_value| value == *possible_value);

                    assert!(maybe_sub_index.is_some());

                    sub_values.remove(maybe_sub_index.unwrap());
                } else {
                    unreachable!("only Message::WithContent expected")
                }
            })
            .await;

        assert!(values
            .lock()
            .await
            .iter()
            .all(|sub_values| sub_values.is_empty()));
    })
    .await;
}
