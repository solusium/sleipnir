use {
    abi_stable::StableAbi,
    sleipnir::{
        ffi::sync::{oneshot::channel, Error},
        runtime::Handle,
    },
};

#[repr(C)]
#[derive(Clone, Debug, Eq, PartialEq, StableAbi)]
enum Message {
    HelloWorld,
}

#[sleipnir::test]
async fn hello_world(_handle: Handle) {
    let (sender, receiver) = channel();

    {
        let send_result = sender.send(Message::HelloWorld);

        assert!(send_result.is_ok());
    }

    let message_result = receiver.await;

    assert!(message_result.is_ok());

    assert_eq!(Message::HelloWorld, message_result.unwrap());
}

#[sleipnir::test]
async fn closed_counterpart(_handle: Handle) {
    {
        let (sender, _) = channel();

        let send_result = sender.send(Message::HelloWorld);

        assert!(send_result.is_err());

        assert_eq!(Error::Disconnected, send_result.unwrap_err());
    }

    let (_, receiver) = channel::<Message>();

    let message_result = receiver.await;

    assert!(message_result.is_err());

    assert_eq!(Error::Disconnected, message_result.unwrap_err());
}
