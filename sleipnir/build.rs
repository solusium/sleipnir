use std::{env::var, path::Path};

fn main() {
    let mainifest_dir = var("CARGO_MANIFEST_DIR").expect("CARGO_MANIFEST_DIR not set");

    let manifest_dir_path = Path::new(&mainifest_dir);

    let mut library_dir = manifest_dir_path
        .parent()
        .expect("expected to be in a workspace space")
        .to_str()
        .expect("expecting UTF-8 compatible path")
        .to_string();

    library_dir.push_str("/target/");

    library_dir.push_str(&var("PROFILE").expect("PROFILE not set"));

    library_dir.push_str("/deps/libsleipnir_test_abi_stable_module.");

    let file_extension = if cfg!(target_os = "windows") {
        "dll"
    } else if cfg!(target_os = "macos") {
        "dylib"
    } else {
        "so"
    };

    library_dir.push_str(file_extension);

    println!(
        "cargo:rustc-env=SLEIPNIR_TEST_ABI_STABLE_MODULE_LIBRARY_PATH={}",
        library_dir
    );
}
