use {
    futures_time::{task::sleep, time::Duration},
    sleipnir::{ffi, runtime::Handle},
    tracing::{info, subscriber::set_global_default},
    tracing_subscriber::FmtSubscriber,
};

async fn run() {
    info!("tick");

    sleep(Duration::from_secs(1)).await;

    info!("tock")
}

#[sleipnir::main]
async fn main(handle: Handle) {
    let subscriber = FmtSubscriber::new();

    set_global_default(subscriber).expect("couldn't set the global tracing subscriber");

    let ffi_handle = ffi::runtime::Handle::new(handle.clone());

    let ffi_join_handle = ffi_handle.spawn(run());

    ffi_join_handle.await.expect("the async task panicked");
}
