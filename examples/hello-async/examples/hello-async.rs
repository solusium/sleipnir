use {
    futures_time::{task::sleep, time::Duration},
    sleipnir::runtime::Handle,
    tracing::{info, subscriber::set_global_default},
    tracing_subscriber::FmtSubscriber,
};

async fn run() {
    info!("tick");

    sleep(Duration::from_secs(1)).await;

    info!("tock")
}

#[sleipnir::main]
async fn main(handle: Handle) {
    let subscriber = FmtSubscriber::new();

    set_global_default(subscriber).expect("couldn't set the global tracing subscriber");

    handle.spawn(run()).await.expect("the async task panicked");
}
