# Sleipnir

The sleipnir crate offers an abstraction over [Tokios](https://github.com/tokio-rs/tokio)
async/await executor.

