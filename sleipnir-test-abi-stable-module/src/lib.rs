#![warn(clippy::all, clippy::pedantic, clippy::nursery, clippy::cargo)]

#[cfg(feature = "test")]
mod tests {
    use {
        abi_stable::{
            export_root_module,
            prefix_type::PrefixTypeTrait,
            sabi_extern_fn,
            std_types::{RResult, RResult::RErr, RString},
        },
        async_ffi::FfiFuture,
        sleipnir::ffi::{runtime::Handle, task::JoinHandle},
        sleipnir_test_abi_stable_module_declaration::tests::{Module, ModuleRef},
        std::{panic::catch_unwind, str::FromStr},
    };

    #[inline]
    #[export_root_module]
    fn instantiate_root_module() -> ModuleRef {
        Module { current, spawn }.leak_into_prefix()
    }

    #[inline]
    #[sabi_extern_fn]
    #[allow(improper_ctypes_definitions)]
    fn current() -> RResult<Handle, RString> {
        let result = catch_unwind(sleipnir::runtime::Handle::current);

        match result {
            Ok(_) => unreachable!(),
            Err(payload) => RErr::<Handle, RString>(
                RString::from_str(
                    payload
                        .downcast_ref::<&str>()
                        .map_or("a panic occurred with unknown payload", |panic_message| {
                            panic_message
                        }),
                )
                .unwrap(),
            ),
        }
    }

    #[inline]
    #[sabi_extern_fn]
    #[allow(improper_ctypes_definitions)]
    fn spawn(handle: Handle, future: FfiFuture<i32>) -> JoinHandle<i32> {
        handle.spawn(future)
    }
}
